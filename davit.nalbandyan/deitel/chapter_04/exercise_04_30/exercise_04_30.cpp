#include <iostream>

int
main()
{
    double pi = 3.14159, radius;
    std::cout << "Input the radius of the circle: ";
    std::cin >> radius;
    double diameter = 2 * radius;
    double circumference = 2 * pi * radius;
    double area = pi * radius * radius;
    std::cout << "\nDiameter of the circle is " << diameter;
    std::cout << "\nCircumference of the circle is " << circumference;
    std::cout << "\nArea of the circle is " << area << std::endl;
    return 0;
}
