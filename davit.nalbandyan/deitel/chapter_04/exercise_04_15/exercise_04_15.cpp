#include <iostream>

int
main()
{
    double sales = 0;
    while (-1 != sales){
        std::cout << "\nEnter sales in dollars (-1 If the input is completed): ";
        std::cin >> sales;
        if (-1 == sales){
            break;
        } else if (0 > sales){
            std::cout << "\nInput not valid, closing the program\n";
            return 1;
        }
        double profit = 200 + 9 * sales / 100;
        std::cout << "\nProfit: " << profit;
    }
    return 0;
}
