#include <iostream>
#include <cmath>

int
main()
{
    int number, degree = 1, result = 0;
    std::cout << "\nInput number only with zeroes and ones: ";
    std::cin >> number;
    if (number < 0){
        std::cout << "Input not valid, positive number only" << std::endl;
        return 1;
    }
    while (number > 0) {
        int binaryDigit = number % 10;
        if (binaryDigit > 1) {
            std::cout << "\nError input number should be only with zeroes and ones: ";
            return 1;
        }
        int decimalDigit = binaryDigit * degree;
        result += decimalDigit;
        degree *= 2;
        number /= 10;
    }
    std::cout << "The Result is: " << result << std::endl;
    return 0;
}
