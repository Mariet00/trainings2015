#include <iostream>

int
main()
{
    int passes = 0, failures = 0, studentCounter = 0, result;
    while (studentCounter <= 10) {
        std::cout << "\nEnter result (1 = pass, 2 = fail): ";
        std::cin >> result;
        if (1 == result) {
            ++passes;
            ++studentCounter;
        } else if (2 == result) {
            ++failures;
            ++studentCounter;
        } else {
            std::cout << "\nInput not valid, please input 1 or 2";
        }
    }
    std::cout << "Passed " << passes << "\nFailed " << failures << std::endl;
    if (passes > 8) {
        std::cout << "Raise tuition" << std::endl;
    }
    return 0;
}
