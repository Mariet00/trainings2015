#include <iostream>

int
main()
{
    int length;
    std::cout << "\nInput the length of the square (1-20): ";
    std::cin >> length;
    if (length <= 0) {
        std::cout << "\nInput not valid, please input in the range (1 - 20)";
        return 1;
    }
    if (length > 20) {
        std::cout << "\nInput not valid, please input in the range (1 - 20)";
        return 1;
    }
    int row = 1;    
    while (row <= length) {
        int column = 1;
        while (column <= length) {
            if (1 == row) {
                std::cout << "*";
            } else if (length == row) {
                std::cout << "*";
            } else if (1 == column) {
                std::cout << "*";
            } else if (length == column) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
            ++column;
        }
        std::cout << std::endl;
        ++row;
    }
    return 0;
}
