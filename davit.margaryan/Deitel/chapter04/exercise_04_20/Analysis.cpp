/// Analysis.cpp
/// Implementation element functions of Analysis class
/// which analyses the exam results

#include "Analysis.hpp" /// defintion of Analysis class
#include <iostream> /// allows program to perform input and output

/// process examination results of 10 students
void
Analysis::processExamResults()
{
    /// initialization of variables at the declaration
    int passes = 0; /// the number of passed
    int failures = 0; /// the number of failed
    int studentCounter = 1; /// students counter

    /// process 10 students using the meter loop
    while (studentCounter <= 10) {
        int result = 0; /// one exam result (1 - passed, 2 - not passed)

        /// prompt the user to enter the value
        std::cout << "Enter result (1-pass, 2-fail): ";
        std::cin >> result; /// enter result

        if (1 == result) { /// if result is equal to 1
            passes = passes + 1; /// increase passes
        } else if(2 == result) { /// else
            failures = failures + 1; /// increase failures
        } else {
            std::cout << "Invalid result! ";
            continue;
        }

        /// increase student count, so the loop could end
        studentCounter = studentCounter + 1;
    } /// end while

    /// stage of completion; bring the number who have passed and failed
    std::cout << "Passed: " << passes 
              << "\nFailed: " << failures << std::endl;

    /// to determine wheter more then eight students passed
    if(passes > 8) {
        std::cout << "Raise tution." << std::endl;
    } /// end if
} /// end function processExamResults
