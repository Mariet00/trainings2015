/// main.cpp

#include "Analysis.hpp" /// defintion of Analysis class
#include <iostream> /// allows program to perform input and output
#include <iomanip> /// allows program to perform stream manipulator

/// function main begins program execution
int
main()
{
    Analysis aplication; /// create object aplication
    aplication.processExamResults(); /// call process function

    return 0; /// program ends successfully
} /// end function main
