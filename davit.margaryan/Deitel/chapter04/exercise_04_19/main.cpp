/// The program finds two largest numbers during input

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// allows program to perform stream manipulator

/// function main begins program execution
int
main()
{
    int counter = 1;
    int largest1 = -2147483647;
    int largest2 = largest1;

    while (counter <= 10) {
        int number = 0;
        /// promt user to input number
        std::cout << "Enter number " << counter << ": ";
        std::cin >> number; /// input number

        /// find two largest numbers
        if(number > largest1) {
            largest2 = largest1;
            largest1 = number;
        } else if (number > largest2) {
            largest2 = number;
        }
        ++counter;
    }
    std::cout << "Largest1: " << largest1 << std::endl
              << "Largest2: " << largest2 << std::endl;

    return 0; /// program ends successfully
} /// end function main
