/// The program gets hours and hourly rate from user and calculate salary

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// allows program to perform stream manipulator

/// function main begins program execution
int
main()
{
    /// initialize hours
    double hours = 0;

    /// prompt user to enter hours
    std::cout << "Enter hours worked (-1 to end): ";
    std::cin >> hours; /// input hours

    /// while hours is not -1
    while (hours != -1) {
        /// if hours is invalid
        if (hours < 0) {
            std::cout << "Error 1: Hours can not be negative!\n";
            return 1; /// program end with error 1
        } /// end if
        
        /// initialize rate
        double rate = 0;
        /// prompt user to input rate
        std::cout << "Enter hourly rate of employee ($00.00): ";
        std::cin >> rate; /// input rate
        if (rate < 0) { /// if rate is invalid
            std::cout << "Error 2: Rate can not be negative!\n";
            return 2; /// program ends with errror 2
        } /// end if
        
        /// initialize and calculate salary
        double salary = hours * rate;
        if (hours > 40) {
            salary += (hours - 40) * rate / 2;
        }

        /// print salary
        std::cout << "Salary is: " << std::fixed
              << salary << std::endl;

        /// prompt user to enter hours
        std::cout << "Enter hours worked (-1 to end): ";
        std::cin >> hours; /// input hours
    } /// end while

    return 0; /// program ends successfully
} /// end function main
