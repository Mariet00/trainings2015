/// The program determines wheter a department store custemer
/// has esceeded the credit limit on a change account

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// allows program to perform stream manipulator

/// function main begins program execution
int
main()
{
    /// initialization of account number
    int accountNumber = 0;

    /// Prompt user to enter account number
    std::cout << "Enter account number (or -1 to quit): ";
    std::cin >> accountNumber; /// input account number

    /// while account number is not equal to minus one
    while (accountNumber != -1) {
        /// if account number is invalid
        if (accountNumber < 0) {
            std::cout << "Error 1: Account number must be positive!\n";
            return 1; /// program ends with error 1
        } /// end if

        /// initialize beginning balances
        double beginningBalance = 0;
        /// prompt user to input begining balance
        std::cout << "Enter baginning balance: ";
        std::cin >> beginningBalance; /// input beginning balance
        if (beginningBalance < 0) { /// if begining balance is invalid
            std::cout << "Error 2: Beginning balance can not be negative!\n";
            return 2; /// program ends with error 2
        } /// end if

        /// initialize charge 
        double charge = 0;
        /// prompt user to input total charges
        std::cout << "Enter total charges: ";
        std::cin >> charge;/// input charge
        if (charge < 0) { /// if charge is invalid
            std::cout << "Error 3: Charge can not be negative!\n";
            return 3; /// program ends with error 3
        } /// end if

        /// initialize credit 
        double credit = 0;
        /// prompt user to input credits
        std::cout << "Enter total credits: ";
        std::cin >> credit; /// input credit
        if (credit < 0) { /// if credit is invalid
            std::cout << "Error 4: Credit can not be negative!\n";
            return 4; /// program ends with error 4
        } /// end if

        /// initialize creditLimit
        double creditLimit = 0;
        /// prompt user enter credit limit
        std::cout << "Enter credit limit: ";
        std::cin >> creditLimit; /// input credit limit
        if (creditLimit <= 0) { /// if credit limit is invalid
            std::cout << "Error 5: Credit limit can not be negative!\n";
            return 5; /// program ends with error 5
        } /// end if

        /// initialize new balance and output it
        double newBalance = beginningBalance + charge - credit;
        std::cout << "New balance is: " << std::fixed << newBalance << std::endl;
        /// if new balance is greahter thenn credit limit
        if (newBalance > creditLimit) {
            /// output data 
            std::cout << "Account: " << std::fixed << accountNumber << std::endl
                      << "Credit Limit: " << std::fixed << creditLimit << std::endl
                      << "Balance: " << std::fixed << newBalance << std::endl
                      << "Credit Limit Exceeded." << std::endl;
        } /// end if

        /// prompt user to enter account number
        std::cout << "Enter account number (or -1 to quit): ";
        std::cin >> accountNumber; /// input account number
    } /// end while

    return 0; /// program ends successfully
} /// end function main
