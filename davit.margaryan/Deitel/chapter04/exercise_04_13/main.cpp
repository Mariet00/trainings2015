/// The program calculates and display the miles per galon
/// abtained for each trip and print the combined miles.

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// parameterized stream manipulators

/// function main begins program execution
int
main()
{
    /// initialization of data
    double miles = 0, totalMiles = 0,
           totalGallons = 0; 

    std::cout << "\nEnter miles driven (-1 to quit): ";
    std::cin >> miles; /// input gallons

    while (miles != -1) { /// continue  while miles is not equal -1
        if (miles < 0) {
            std::cout << "Error 1: miles can not be negative except -1!\n";
            return 1;
        }
        double gallons;
        std::cout << "Enter gallons used: ";
        std::cin >> gallons; /// input gallons
        if (gallons <= 0) {
            std::cout << "Error 2: Gallons should be positive!\n";
            return 2;
        }

        /// calculate miles per gallons
        double mpg = miles / gallons;
        std::cout << "miles/gallons this trip: " << std::fixed  
                  << std::setprecision(6)
                  << mpg << std::endl; /// output result
        totalMiles += miles; /// calculate totalMiles

        totalGallons += gallons; /// calculate totalGalones
        /// calcucate totalMiles per totalGallons
        double totalMpg = totalMiles / totalGallons;
        std::cout << "Total miles/gallons: " 
                  << std::fixed <<  std::setprecision(6)
                  << totalMpg << std::endl; /// output result

        std::cout << "\nEnter miles driven (-1 to quit): ";
        std::cin >> miles; /// input gallons
    } /// end while

    return 0; /// program ends successfully
} /// end funtion main
