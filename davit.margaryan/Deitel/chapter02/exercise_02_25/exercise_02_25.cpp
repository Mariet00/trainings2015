/// The program reades in to integers and determines
/// and prints if the first is the multiple of second.

#include <iostream> /// allwos program to perform input and output

/// function main begins program execution
int
main()
{
    /// initialization of data
    int number1 = 0, number2 = 0;

    /// input data
    std::cout << "Enter two integers: ";
    std::cin >> number1 >> number2;

    if(0 == number2) { /// check if denominator is zero
        std::cout << "The denominator can not be zero!" << std::endl;
        return 1; /// program ends
    } /// end if
    if(number1 % number2 == 0) { /// if number1 is multiple of number2
        std::cout << "The " << number1 << " is multiple of "
                            << number2 << "." << std::endl;
    } /// end if
    if(number1 % number2 != 0) { /// if number1 is not multiple of number2
        std::cout << "The " << number1 <<  " is not multiple of " 
                            << number2 << "." << std::endl;
    } /// end if

    return 0; /// program ends successfully
} /// end function mian
