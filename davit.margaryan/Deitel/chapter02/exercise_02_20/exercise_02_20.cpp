/// Reads in the raius of a circle as an integer
/// and prints the circle's diametor, circumference and area.

#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    /// initialization of data
    int radius = 0;

    /// inout data
    std::cout << "Enter radius: ";
    std::cin >> radius;
    
    if(radius <= 0) { /// if the radius is less or equal then zero
        std::cout << "Radius can not be less or equal then zero!" << std::endl;
        return 0; /// program ends
    } /// end if
    
    /// calculation of diametor, circumference and area
    int diametor = 2 * radius;
    int circumference = diametor * 3.14159;
    int area = radius * radius * 3.14159;

    /// print data
    std::cout << "Diametor: " << diametor << std::endl;
    std::cout << "Circumference: " << circumference << std::endl;
    std::cout << "Area: " << area << std::endl;

    return 0; /// program ends successfully
} /// end function main
