/// main.cpp
/// test class Employee

#include "Employee.hpp" /// includes class Employee definition
#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    /// employes initialization
    Employee employee1("Bill", "Gates", 1000);
    Employee employee2("Steve", "Jobs", 2000);

    /// get salary  
    int firstEmployeeSalary = employee1.getSalary();
    int secondEmployeeSalary = employee2.getSalary();

    /// outputing employees
    std::cout << "First Employee.\n"
              << "First name: " << employee1.getFirstName() << '\n'
              << "Last name: " << employee2.getLastName() << '\n'
              << "Yearly salary: " << 12 * firstEmployeeSalary << '\n'
              << std::endl;
    std::cout << "Second Employee.\n"
              << "First name: " << employee2.getFirstName() << '\n'
              << "Last name: " << employee2.getLastName() << '\n'
              << "Yearly salary: " << 12 * secondEmployeeSalary << '\n'
              << std::endl;

    /// raise 10% employee1 and employee2 slaryes
    employee1.setSalary(firstEmployeeSalary + firstEmployeeSalary * 0.1);
    employee2.setSalary(secondEmployeeSalary + secondEmployeeSalary * 0.1);    

     /// outputing employees after changeing salares
    std::cout << "Outputing after raiseing each salary 10%\n\n"
              << "First Employee.\n"
              << "First name: " << employee1.getFirstName() << '\n'
              << "Last name: " << employee1.getLastName() << '\n'
              << "Yearly salary: " << 12 * employee1.getSalary() << '\n'
              << std::endl;
    std::cout << "Second Employee.\n"
              << "First name: " << employee2.getFirstName() << '\n'
              << "Last name: " << employee2.getLastName() << '\n'
              << "Yearly salary: " << 12 * employee2.getSalary() << '\n'
              << std::endl;
 
    return 0; /// program ends successfully
} /// end function main
