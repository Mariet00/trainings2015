#include <iostream>

int
main()
{
    std::cout << "1 - manager\n"
              << "2 - hourly worker\n"
              << "3 - commission worker\n"
              << "4 - piece worker\n" << std::endl;

    int number;

    std::cout << "Enter the appropriate number (non-positive number to quit): ";
    std::cin  >> number;
    
    while (number > 0){
        double salary;        
        switch (number){
        case 1: {
            std::cout << "Enter the fixed salary of manager: ";
            std::cin  >> salary;
        
            if (salary < 0){
                std::cerr << "Error 1: The fixed salary can't be negative." << std::endl;
                return 1;
            }    
            break;  
        }
        case 2: {
            double fixSalary, time;
            std::cout << "Enter the fixed salary of hourly worker: ";
            std::cin  >> fixSalary;
            std::cout << "Enter working time: ";
            std::cin  >> time;

            if (time < 0 || fixSalary < 0){
                std::cerr << "Error 2: The fixed salary or time can't be negative." << std::endl;
                return 2;
            }
            
            salary = fixSalary * time;
            if (time > 40){
                salary *= 1.5 - 20.0 / time;
            }
            break;             
        }
        case 3: {
            double sales; 
            std::cout << "Enter sales for a week: ";
            std::cin  >> sales;

            if (sales < 0){
                std::cerr << "Error 3: Sales can't be negative." << std::endl;
                return 3;
            }

            salary = sales * 0.057 + 250;
            break;
        }
        case 4: {
            double fixSalary; 
            int amount; 
            std::cout << "Enter the fixed salary of piece worker: ";
            std::cin  >> fixSalary;
            std::cout << "Enter amount of produced items: ";
            std::cin  >> amount;

            if (amount < 0 || fixSalary < 0){
                std::cerr << "Error 4: The fixed salary or amount can't be negative." << std::endl;
                return 4;
            }
                
            salary = amount * fixSalary;
            break;       
        }
        default:
            std::cerr << "Error 5: Incorrect number." << std::endl;
            return 5;  
        }
        std::cout << "Salary: " << salary << std::endl;
        std::cout << "\nEnter the appropriate number (non-positive number to quit): ";
        std::cin  >> number;
    }

    return 0;
}
