#include <iostream>

int
main()
{           
    double total = 0;
    for (int day = 1; day <= 7; ++day){        
        std::cout << "The " << day << " day." << std::endl;
        for (int counter = 1; counter <= 5; ++counter){
            int product, count;
            std::cout << "\nEnter the number of product: ";
            std::cin  >> product;
            std::cout << "Enter the count of product: ";
            std::cin  >> count;

            switch (product){    
            case 1: total += 2.98 * count; break;                                       
            case 2: total += 4.50 * count; break;                      
            case 3: total += 9.98 * count; break;                       
            case 4: total += 4.49 * count; break;                       
            case 5: total += 6.87 * count; break;                       
            default:
                std::cout << "\nWarning: Wrong product number. Ignoring." << std::endl;
                --counter; break;            
            }
        }
    }

    std::cout << "\nFor this week you sale. " << total << std::endl;

    return 0;
}
