#include <iostream>

int
main()
{
    int counter = -1, number = 0;
    double sum = 0.0;    

    do {
        sum += number;

        std::cout << "Enter the number: ";
        std::cin  >> number;
        
        counter++; 
    } while (9999 != number);

    if (0 == counter){
        std::cout << "Error 1: Before 9999 you should enter another number.\nTry again." << std::endl;
        return 1;
    }

    std::cout << "Average = " << sum / counter << std::endl;

    return 0;
}
