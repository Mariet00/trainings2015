#include <iostream>

int
main()
{
    int passes = 0, failures = 0, studentCounter = 1, result;
    
    while (studentCounter <= 10){
        std::cout << "Enter result (1 = pass, 2 = fail): ";
        std::cin  >> result;
    
        if (1 == result){
            ++passes;
            ++studentCounter;
        } else if (2 == result){
            ++failures;
            ++studentCounter;
        } else {
            std::cout << "Warning: The result should be 1 = pass or 2 = fail.\nTry again." << std::endl;
        }
    }

    std::cout << "Passed: " << passes << "\nFailed: " << failures << std::endl;

    if (passes > 8){
        std::cout << "Raise tuition." << std::endl;
    }

    return 0;
}
