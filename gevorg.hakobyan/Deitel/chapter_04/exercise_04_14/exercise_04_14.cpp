#include <iostream>

int 
main()
{
    int account;

    std::cout << "Enter number of account (non-positive number to quit): ";
    std::cin  >> account;
   
    while (account > 0){
        double initialBalance;
        std::cout << "Enter initial balance: ";
        std::cin  >> initialBalance;

        double sumOfExpenses;
        std::cout << "Enter the sum of expenses: ";
        std::cin  >> sumOfExpenses;

        if (sumOfExpenses <= 0){
            std::cout << "Error 1: The sum of expenses can't be non-positive." << std::endl;
            return 1;
        }

        double sumOfParish;
        std::cout << "Enter the sum of parish: ";
        std::cin  >> sumOfParish;

        if (sumOfParish <= 0){
            std::cout << "Error 2: The sum of parish can't be non-positive." << std::endl;
            return 2;
        }

        double creditLimit;
        std::cout << "Enter the limit of the credit: ";
        std::cin  >> creditLimit;

        if (creditLimit <= 0){
            std::cout << "Error 3: Credit can't be non-positive." << std::endl;
            return 3;
        }

        double newBalance = initialBalance + sumOfExpenses - sumOfParish;
        std::cout << "New balance: " << newBalance;

        if (newBalance > creditLimit){
            std::cout << "\nAccounts: " << account;
            std::cout << "\nThe limit of the credit: " << creditLimit;
            std::cout << "\nBalance: " << newBalance;
            std::cout << "\nThe limit of the credit exceeded.";
        }
                
        std::cout << "\n\nEnter number of account (non-positive number to quit): ";
        std::cin  >> account;
    }
    return 0;
}
