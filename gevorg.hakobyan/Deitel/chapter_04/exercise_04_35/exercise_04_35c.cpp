#include <iostream>

int
main()
{
    int number;

    std::cout << "Enter the number (none-negative number): " ;
    std::cin  >> number;

    if (number < 0){
        std::cout << "Error 1: The number can't be negative.\nTry again." << std::endl;
        return 1;
    }

    if (0 == number){
        std::cout << "e = " << 2 << std::endl;
        return 0;
    }

    double x;

    std::cout << "Enter the x: " ;
    std::cin  >> x;
    
    double current = 1, factorial = 1, e = 1, x1 = x;
    while (current <= number){
        factorial *= current;
        e += x / factorial;
        ++current;
        x *= x1;
    }
        
    std::cout << "e = " << e << std::endl;
    
    return 0;
   
}
