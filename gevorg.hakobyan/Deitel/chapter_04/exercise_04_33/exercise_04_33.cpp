#include <iostream>

int
main()
{
    double side1, side2, side3;

    std::cout << "Enter the all sides of triangle." << std::endl;
    std::cout << "First side: ";
    std::cin  >> side1;
    if (side1 < 0){
        std::cout << "Error 1: The side can't be negative.\nTry again." << std::endl;
        return 1;
    }
    std::cout << "Second side: ";
    std::cin  >> side2;
    if (side2 < 0){
        std::cout << "Error 1: The side can't be negative.\nTry again." << std::endl;
        return 1;
    }
    std::cout << "Third side: ";
    std::cin  >> side3;
    if (side3 < 0){
        std::cout << "Error 1: The side can't be negative.\nTry again." << std::endl;
        return 1;
    }
    
    side1 *= side1;
    side2 *= side2;
    side3 *= side3;

    if (side1 == side2 + side3){
        std::cout << "Yes, you can build from this sides rectangular triangle." << std::endl;
    } else if (side2 == side1 + side3){
        std::cout << "Yes, you can build from this sides rectangular triangle." << std::endl;
    } else if (side3 == side1 + side2){
        std::cout << "Yes, you can build from this sides rectangular triangle." << std::endl;
    } else {
        std::cout << "No, you can't build from this sides rectangular triangle." << std::endl;
    }
    
    return 0;
}
