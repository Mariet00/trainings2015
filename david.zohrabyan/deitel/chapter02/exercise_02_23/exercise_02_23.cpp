/// This program reads out five integers,defines and prints the biggest and the smallest of them
#include <iostream> /// standard input output
 
/// function main begins program execution
int 
main()
{
    int number1, number2, number3, number4, number5, min, max; /// announcement of variables
 
    std::cout << "number1: " << std::endl; /// prompt user for data
    std::cin >> number1; /// read number from user into number1
 
    std::cout << "number2: " << std::endl; /// prompt user for data
    std::cin >>  number2; /// read number from user into number2
 
    std::cout << "number3: " << std::endl; /// prompt user for data
    std::cin >>  number3; /// read number from user into number3

    std::cout << "number4: " << std::endl;
    std::cin >> number4;
    
    std::cout << "number5: " << std::endl;
    std::cin >> number5;
       
    min = number1;
    if (min > number2) {
        min = number2;
    }
    if (min > number3) {
        min = number3;
    }
    if (min > number4) {
        min = number4;
    }
    if (min > number5) {
        min = number5;
    }
    max = number1;
    if (max < number2) {
        max = number2;
    }
    if (max < number3) {
        max = number3;
    }
    if (max < number4) {
        max = number4;
    }
    if (max < number5) {
        max = number5;
    }
    std::cout << "The min.value is " << min << std::endl;
    std::cout << "The max.value is " << max << std::endl;

    return 0;///program ended successfully
}///end function main
 











