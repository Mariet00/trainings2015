/// This program prints numbers from 1 to 4 on the same line so the next numbers are divided by an ody gap
#include <iostream> /// standard input and output
 
/// function main begins program execution 
int
main()
{
    std::cout << "1 2 3 4" << std::endl; /// output four numbers;end line
    std::cout << "1 " << "2 " << "3 " << "4 " << std::endl; /// output for numbers;end line
    std::cout << "1 " ; /// output one number
    std::cout << "2 " ; /// output one number
    std::cout << "3 " ; /// output one number
    std::cout << "4 " << std::endl; /// output one number;endl line
 
    return 0; /// program ended successefully 
} /// end function main
