/// This program accepts three integers and prints the sum,average value,work,samaller and bigger of these numbers
#include <iostream> /// standard input output

/// function main begins program execution
int
main() 
{
    int a, b, c, d, max, min; /// announcement of variables
 
    std::cout << "Enter three various integers "; /// prompt user for data
    std::cin >> a >> b >> c; /// read number from user into "a","b","c"
    
    d = a + b + c ; /// add the integers;store result in "d"
    std::cout << "The sum is equal: " << d << std::endl; /// display "d";end line 
 
    d = (a + b + c) / 3 ; /// considers average value of integers;store result in "d"
    std::cout << "Average value is equal: " << d << std::endl; /// display "d";end line

    d = a * b * c ; /// multiplicate the integers;store result in "d"
    std::cout << "Work is equal: " << d << std::endl; /// display "d";end line

    min = a;
    if (min > b) {
        min = b;
    }
    if (min > c) {
        min = c;
    } 
    max = a;
    if (max < b) {
        max = b;
    }
    if (max < c) {
        max = c;
    }
    std::cout << "The min. value is " << min << std::endl;
    std::cout << "The max. value is " << max << std::endl;

    return 0; /// program ended successfully
} /// end function main











