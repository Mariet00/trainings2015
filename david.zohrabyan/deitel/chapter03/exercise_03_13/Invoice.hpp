/// The heading file of Invoice
#include <string> /// standard string type

class Invoice
{
public:
    ///Constructor initializing
    Invoice(std::string article, std::string description, int quantity, int price);
    
    /// function set Article
    void setArticle(std::string article);
    
    /// function set Description
    void setDescription(std::string description);
    
    /// function set Quantity
    void setQuantity(int quantity);
    
    /// function set Price
    void setPrice(int price);
    
    /// function get Article
    std::string getArticle();
    
    /// function get Description
    std::string getDescription();
    
    /// function get Quantity
    int getQuantity();
    
    /// function get Price
    int getPrice();
    
    /// function get Amount
    int getInvoiceAmount();
    
private:
    /// member variable of Article
    std::string article_;
    
    /// member variable of Description
    std::string description_;
    
    /// member variable of Quantity
    int quantity_;
    
    /// member variable of Price
    int price_;

};
