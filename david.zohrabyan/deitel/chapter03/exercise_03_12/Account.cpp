#include <iostream> /// Definitions Account element functions
#include "Account.hpp" /// Implementation of design’s Account

Account::Account(int money) /// the constructor initializes parameter
{
    if(money >= 0) { /// checks the imported sum
        balance_ = money;
    }
    if(money < 0) { /// if the sum is bigger than cash
        balance_ = 0;
        std::cout << "The initial balance " << money << " is invalid. It is set to 0" << std::endl;
    }
} 

void 
Account::credit(int credit) /// fuction adds the sum
{
    balance_ += credit;  
}

void 
Account::debit(int debit) /// function withdraws the sum
{
    if(debit > balance_) { /// checks if the requested sum exceeds the initial sum
        std::cout << "The requested sum exceeds balance." << std::endl;
        std::cout << "Your balance is: " << getBalance() << std::endl;
    }

    if(debit <= balance_) { /// checks if the requested sum doesn`t exceeds the initial sum
        balance_ -= debit;
    }   
}

/// function returns the initial sum
int 
Account::getBalance()
{
    return balance_;
} 
    
