#include <iostream> /// Definitions Invoice element functions
#include "Date.hpp" /// Implementation of design’s Date

Date::Date(int month, int day, int year)
{
    /// initializes a set-function call
    setMonth(month);
    setDay(day);
    setYear(year);
}

void
Date::setMonth(int month) /// funcion set the month of Date
{  
    if(month >= 1) {
        if(month <=12) {
            month_ = month;
        }
    }    
    if(month < 1) {
        month_ = 1;
    }
    if(month > 12) {
        month_ = 12;
    }    
}

void
Date::setDay(int day) /// function set the day of Date
{
    day_ = day;
}

void
Date::setYear(int year) /// function set the year of Date
{
    year_ = year;
}

int
Date::getMonth() /// function get the month of Date
{
    return month_;
}

int
Date::getDay() /// function get the day of Date
{
    return day_;
}

int
Date::getYear() /// function get the year of Date
{
    return year_;
}

void
Date::displayDate() /// function display the Date
{
    std::cout << getMonth() << "/" << getDay() << "/" << getYear() << ":" << std::endl;
}
