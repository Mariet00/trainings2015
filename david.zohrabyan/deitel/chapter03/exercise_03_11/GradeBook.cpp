#include <iostream> /// Definitions GradeBook element functions
#include "GradeBook.hpp" /// Implementation of design’s GradeBook

GradeBook::GradeBook(std::string name, std::string teacher) /// the constructor initializes two parameters
{
    setCourseName(name); /// initializes a set-function call
    setTeacherName(teacher);
}

void 
GradeBook::setCourseName(std::string name) 
{
    courseName_ = name; /// saving the name of course into object
}

std::string GradeBook::getCourseName() /// function receives names of a course
{
    return courseName_; /// returns object courseName
}

void 
GradeBook::displayMessage() /// outputs the message greeting to the user of GradeBook
{
    std::cout << "Welcome to the grade book for " << getCourseName() << "!" << std::endl; /// causes getCourseName for receiving courseName
    std::cout << "This course is presented by: " << getTeacherName() << "!" << std::endl; /// causes getTeacherName for receiving teacherName
}

void 
GradeBook::setTeacherName(std::string teacher)
{
    teacherName_ = teacher; /// saving the name of teacher into object
}

std::string GradeBook::getTeacherName() /// function receives names of teacher
{
    return teacherName_; /// returns object teacherName
}


