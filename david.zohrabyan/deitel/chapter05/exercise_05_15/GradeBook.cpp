#include "GradeBook.hpp"
#include <iostream> 
#include <cstdio> 
 
GradeBook::GradeBook(std::string name) 
{ 
    setCourseName(name); 
    aCount = 0;
    bCount = 0; 
    cCount = 0; 
    dCount = 0; 
    fCount = 0;
}
 
void
GradeBook::setCourseName(std::string name) 
{ 
    if (name.length() <= 25) {
        courseName = name; 
    } else {
        courseName = name.substr(0, 25);
        std::cout << "Name \"" << name << "\" exceeds maximum length B5).\n" << "Limiting courseName to first 25 characters. \n" << std::endl; 
    } 
} 
 
std::string
GradeBook::getCourseName() 
{ 
    return courseName; 
} 
 
void
GradeBook::displayMessage() 
{ 
    std::cout << "Welcome to the grade book for\n" << getCourseName() << "!\n" << std::endl; 
}
 
void 
GradeBook::inputGrades() 
{ 
    int grade; 
    std::cout << "Enter the letter grades." << std::endl << "Enter the EOF character to end input." << std::endl; 

    while ((grade = std::cin.get()) != EOF) {
        switch (grade) { 
        case 'A': case 'a': ++aCount; break;
        case 'B': case 'b': ++bCount; break;
        case 'C': case 'c': ++cCount; break;
        case 'D': case 'd': ++dCount; break;
        case 'F': case 'f': ++fCount; break;
        case '\n': case '\t': case ' ': break;
        default: 
            std::cout << "Incorrect letter grade entered ( " << static_cast<char>(grade) << " )" << std::endl; 
            break; 
        }  
    } 
}  
 
void 
GradeBook::displayGradeReport() 
{  
    std::cout << "\n\nNumber of students who received each letter grade: "
              << "\nA " << aCount
              << "\nB " << bCount
              << "\nC " << cCount
              << "\nD " << dCount
              << "\nF " << fCount
              << std::endl;
 
    double average = (4 * aCount + 3 * bCount + 2 * cCount + 1 * dCount + 0 * fCount);
    average /= (aCount + bCount + cCount + dCount + fCount);

    std::cout << "Average grade is " << average << std::endl;

}

