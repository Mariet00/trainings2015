#include <iostream>

int
main()
{   
    for (int counter = 0; counter < 5; ++counter) {
        int asterisk;
        std::cout << "Enter number from 1 to 30: ";
        std::cin >> asterisk;

        if (asterisk < 1 || asterisk > 30) {
            std::cerr << "Error 1. Number must be from 1 to 30." << std::endl;
            return 1;
        }   
        while (asterisk > 0) {
            std::cout << "*";
            --asterisk;
        }
        std::cout << std::endl;
    
    }

    return 0;
}
