#include <iostream>
#include <iomanip>

int
main()
{
    std::cout << "Decimal" << std::setw(10) << "Binary" << std::setw(8) << "Octal" << std::setw(14) << "Hexadecimal" << std::endl;
    for (int decimal = 1; decimal <= 256; ++decimal) {
        int decimalCounter = decimal;
        int counter = 256;
        std::cout << std::dec << decimal << std::setw(8);
        do {
            int binary;
            if (decimalCounter < counter) {
                binary = 0;
            } else {
                binary = 1;
                decimalCounter -= counter;
            }
            std::cout << binary;
            counter /= 2;
         }while (counter != 0);
         std::cout << std::setw(8) << std::oct << decimal << std::setw(14) << std::hex << decimal << std::endl;   
     }
     return 0;
}     
