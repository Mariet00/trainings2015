#include <iostream>

int
main()
{
    int sum = 0, number = 0, quantity = -1;
    do {
        sum += number;
        std::cout << "Enter the integer: ";
        std::cin >> number;
        ++quantity;
    } while(9999 != number); 
        
    if(0 == quantity) {
        std::cerr << "Error 1. No entered integer.";
        return 1;
    } else {
        std::cout << sum / quantity << std::endl;
    }
    
    return 0;
}
