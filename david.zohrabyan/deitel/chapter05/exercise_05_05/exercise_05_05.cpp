#include <iostream>

int
main()
{
    int sequence; 
    std::cout << "Enter sequence: ";
    std::cin >> sequence;
    if(sequence <= 0) {
        std::cerr << "Error 1. The sequence must be positive." << std::endl;
        return 1;
    } 
    
    int sum = 0;
    for(int counter = 0; counter < sequence; ++counter)
    {
        int number;
        std::cout << "Enter the integer number: ";
        std::cin >> number;
        sum += number;
    }
    
    std::cout << sum << std::endl;
    
    return 0;
}
