#include <iostream>

int
main()
{
    int counter = 0, largest = -2147483648;
    while (counter < 10) {
        int number;
        std::cout << "\nEnter number: ";
        std::cin >> number;
        if (number > largest) {
            largest = number;
        }
        ++counter;
    }
    std::cout << "\nThe largest number is: " << largest << std::endl;
    return 0;
}
