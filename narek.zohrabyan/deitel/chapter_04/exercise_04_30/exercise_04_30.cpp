#include <iostream>

int
main()
{
    double radius;
    std::cout << "Enter radius: ";
    std::cin >> radius;
    if (radius <= 0) {
        std::cout << "Error 1: Invalid radius parameter. Try again!" << std::endl;
        return 1;
    }
    double pi = 3.14159;
    std::cout << "Diameter = " << 2 * radius << std::endl;
    std::cout << "Circumference = " << 2 * pi * radius << std::endl;
    std::cout << "Area of a circle = " << pi * radius * radius << std::endl;
    return  0;
}
