#include <iostream>

int
main()
{
   int i = 1;
   while (8 >= i) {        
       if (0 == i % 2) {
           std::cout << " ";
       }
       int j = 1;
       while (8 >= j) {
           std::cout << "* ";
           ++j;
       }
       std::cout << std::endl;
       ++i;
   }
   return 0;
}
