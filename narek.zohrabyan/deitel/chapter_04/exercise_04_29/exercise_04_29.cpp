#include <iostream>

int
main()
{
    int i = 1;
    while (true) {
        i *= 2;
        std::cout << i << std::endl;
       /* Our system int variable occupies 4 bytes of memory or 32-bit space. 2 ^ 31 = 1000 0000.... 0000 as the first bit defines a sign so the moment becomes the negative number. 2 ^ 32 levels the first bit is excluded from memory. As there are only zero, we will have zero as a result of the subsequent actions. Permanently repeated a cycle zero continuously prints */
    }
    return 0;
}
