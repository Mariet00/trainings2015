#include <iostream>

int
main()
{
    int number;
    std::cout << "Enter number: ";
    std::cin >> number;
    if (number < 0) {
        std::cout << "Error 1: Invalid number. Try again!" << std::endl;
        return 1;
    }
    double x;
    std::cout << "Enter x: ";
    std::cin >> x;
    if (x < 0) {
        std::cout << "Error 2: Invalid x. Try again!" << std::endl;
        return 2;
    }
    int counter = 1, factorial = 1;
    double eulerNumber = 1, degree = x; 
    while (counter <= number) {
        factorial *= counter;
        eulerNumber += x / factorial;
        ++counter;
        degree *= x;
    }
    std::cout << "pow(e, x) = " << eulerNumber << std::endl;   
    return 0;
}
