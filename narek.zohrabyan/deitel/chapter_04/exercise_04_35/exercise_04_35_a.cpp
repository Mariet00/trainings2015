#include <iostream>

int
main()
{
    int number;
    std::cout << "Enter number: ";
    std::cin >> number;
    if (number < 0) {
        std::cout << "Error 1: Invalid number. Try again!" << std::endl;
        return 1;
    }
    int counter = 1, factorial = 1; 
    while (counter <= number) {
        factorial *= counter;
        ++counter;
    }
    std::cout << number << "! = " << factorial << std::endl;
    return 0;
}
