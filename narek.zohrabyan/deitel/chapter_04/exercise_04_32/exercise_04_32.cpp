#include <iostream>

int
main()
{
    double side1, side2, side3;
    std::cout << "Please import triangle sides: ";
    std::cin >> side1 >> side2 >> side3;
    if (side1 <= 0) {
        std::cout << "Error 1: Triangle side can not be zero or the negative․ Try again!" << std::endl;
        return 1;
    }
    if (side2 <= 0) {
        std::cout << "Error 1: Triangle side can not be zero or the negative․ Try again!" << std::endl;
        return 1;
    }
    if (side3 <= 0) {
        std::cout << "Error 1: Triangle side can not be zero or the negative․ Try again!" << std::endl;
        return 1;
    }
    if (side1 + side2 <= side3) {
        std::cout << "Error 2: They can't represent the triangle sides. Try again!" << std::endl;
        return 2;
    } else if (side1 + side2 <= side3) {
        std::cout << "Error 2: They can't represent the triangle sides. Try again!" << std::endl;
        return 2;
    } else if (side2 + side3 <= side1) {
        std::cout << "Error 2: They can't represent the triangle sides. Try again!" << std::endl;
        return 2;
    } else {
        std::cout << "They can represent the triangle sides." << std::endl;
    }
    return 0;
}
