#include "Invoice.hpp"
#include <iostream>

Invoice::Invoice(std::string article, std::string description, int quantity, int price)
{
    setArticle(article);
    setDescription(description);
    setQuantity(quantity);
    setPrice(price);
}

void
Invoice::setArticle(std::string article)
{
    article_ = article;
}

std::string
Invoice::getArticle()
{
    return article_;
}

void
Invoice::setDescription(std::string description)
{
    description_ = description;
}

std::string
Invoice::getDescription()
{
    return description_;
}

void
Invoice::setQuantity(int quantity)
{
    if (quantity >= 0) {
    
        quantity_ = quantity;
    }
    if (quantity < 0) {
   
        quantity_ = 0;
        std::cout << "The entered quantity is invalid! It is reset to 0" << std::endl;
    }
}

int
Invoice::getQuantity()
{
    return quantity_;
}

void
Invoice::setPrice(int price)
{
    if (price >= 0)
    {
        price_ = price;
    }
    if (price < 0)
    {
        std::cout << "Entered price is invalid!" << std::endl;
    }
}

int
Invoice::getPrice()
{
    return price_;
}

int
Invoice::getInvoiceAmount()
{
    return quantity_ * price_;
}
