#include "Date.hpp"
#include <iostream>

int
main()
{
    Date date(6, 6, 2016);
    std::cout << "Testing displayDate function: " ;
    date.displayDate();
    std::cout << "Testing month condition where month = 13: ";
    date.setMonth(13);
    date.displayDate();
    std::cout << "Testing month condition where month = 0: ";
    date.setMonth(0);
    date.displayDate();

    return 0;
}
