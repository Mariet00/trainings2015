#include "GradeBook.hpp"
#include <iostream>

int
main()
{
    GradeBook gradebook1("C++", "Artak");
    gradebook1.displayMessage();
  
    
    std::string nameOfTeacher;
    std::string nameOfCourse;

    std::cout << "Please enter course name: " << std::endl;
    std::getline(std::cin, nameOfCourse); 
    gradebook1.setCourseName(nameOfCourse);
    std::cout << "Please enter teacher name: " << std::endl;
    std::getline(std::cin, nameOfTeacher);
    gradebook1.setTeacherName(nameOfTeacher); 
    gradebook1.displayMessage();

    return 0;
}
