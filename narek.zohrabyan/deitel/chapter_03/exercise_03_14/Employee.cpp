#include "Employee.hpp"
#include <iostream>

Employee::Employee(std::string name, std::string surname, int salary)
{
    setName(name);
    setSurname(surname);
    setSalary(salary);
}

void
Employee::setName(std::string name)
{
    name_ = name;
}

std::string
Employee::getName()
{
    return name_;
}

void
Employee::setSurname(std::string surname)
{
    surname_ = surname;
}

std::string
Employee::getSurname()
{
    return surname_;
}

void
Employee::setSalary(int salary)
{
    if (salary >= 0) {
        salary_ = salary;
    }
    if (salary < 0) {
        salary_ = 0;
        std::cout << "Invalid salary!" << std::endl;
    }
}

int
Employee::getSalary()
{
    return salary_;
}
