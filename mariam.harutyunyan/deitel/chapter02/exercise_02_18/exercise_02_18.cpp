#include <iostream>

int
main()
{
    int number1, number2;
    std::cout << "Enter two integers" << std::endl;
    std::cin >> number1 >> number2;
    
    if(number1 > number2) {
        std::cout << "The biggest is " << number1 << std::endl;
    }
    
    if(number2 > number1) {
        std::cout << "The biggest is " << number2 << std::endl;
    }
    
    if(number1 == number2) {
        std::cout << "The integers are equal" << std::endl;
    }
  
    return 0;
}

