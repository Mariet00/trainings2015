#include <iostream>

int
main()
{
     int a, b, c;
     
     std::cout << "Enter variable a: " << std::endl;
     std::cin >> a;
     
     std::cout << "Enter variable b: " << std::endl;
     std::cin >> b;
     
     c = a + b;
     std::cout << "a+b=" << c << std::endl;
     
     c = a * b;
     std::cout << "a*b=" << c << std::endl;
  
     c = a - b;
     std::cout << "a-b=" << c << std::endl;
     
     if (0 != b) {
         c = a / b;
         std::cout << "a/b=" << c << std::endl;
     }
     if (0 == b) {
         std::cout << "Error. It`s imppossible to divide number into zero:" << std::endl;
     }
     return 0;
}
