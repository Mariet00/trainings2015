#include <iostream>

int 
main()
{
    int R;

    std::cout << "R: ";
    std::cin >> R;

    if (R < 0) {
        std::cout << "Error radius can't be minus: ";
    		return 1;
    }
    std::cout << "Area = " << R * R * 3.14159 << std::endl;
    std::cout << "Diametr = " << 2 * R << std::endl;
    std::cout << "Circle = " << 2 * 3.14159 * R << std::endl;

    return 0;
}
